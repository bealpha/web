$(function(){
	$(window).scroll(function (event) {
		var scroll = $(window).scrollTop();
		if(scroll > 300 ){
			$('.top-logo').removeClass("show_it");
			
			$('#btnMenu').removeClass("white").addClass("purple");
		}else{
			$('.top-logo').addClass("show_it");
			$('#btnMenu').removeClass("purple").addClass("white");
		}

	});	

	$('#btnMenu').on('click', function () {
		$('.menu').addClass("show");
	});

	$('.btn-close-menu').on('click', function () {
		$('.menu').removeClass("show");
	});

	$('body').on('click', function (e) {
		var container = $('.menu');
		if($('#btnMenu').is(e.target)){
			return;
		}

		if (!container.is(e.target) && container.has(e.target).length === 0 ){
			$('.menu').removeClass("show");
		}
	});



	$('.show-modal').on('click', function () {
		var modal = $(this).data("modal");
		console.log(modal)
		$('#'+modal).modal('show') ;
	});

	$( window ).resize(function() {
		evaluateScreenSize();
	});
	
	initMap();
	evaluateScreenSize();



	AOS.init();



	$('#btnShowMoreHistory').hover(function(){
		$('html').addClass("show-scroll");
	},function(){
		$('html').removeClass("show-scroll");
	});


	$('#btnEnviarConsulta').on('click', function () {
		var valido = true;
		
		$('#mensajeRespuesta').empty().hide();
		$('#mail,#mensaje').removeClass("error");
		if($('#mail').val() == "" || validaMail($('#mail').val()) == false ){
			valido =false;
			$('#mail').addClass("error");
		}

		if($('#mensaje').val() == ""){
			valido =false;
			$('#mensaje').addClass("error");
		}
		$('#btnEnviarConsulta').text("enviando ... ").prop("disabled",true);
		if(valido){
			$.ajax({
				url: 'mail.php',
				type: 'post',
				data: {
					mail:$('#mail').val(),
					mensaje:$('#mensaje').val()
				},
				dataType:"JSON",
				success: function (data) {
					console.log(data);
					var clase = 'error';
					if( data.status == "ok" ){
						$('#mail').val("");
						$('#mensaje').val("");
						clase = data.status;
					}
					$('#mensajeRespuesta').addClass(clase).html(data.msj).show();
					$('#btnEnviarConsulta').text("enviar").prop("disabled",false);
				},
				error: function (data) {
					$('#btnEnviarConsulta').text("enviar").prop("disabled",false);
					console.log(data);
				}
			});
		}else{
			$('#btnEnviarConsulta').text("enviar").prop("disabled",false);
			$('#mensajeRespuesta').addClass('error').html("Todos los campos son obligatorios").show();
		}

		
	});




});	

function evaluateScreenSize(){
	console.log("wv")
	if(isMovil()){
		$('.web-container').hide();
		window.location.href = 'https://notariadigital.cl/';
		return;
	}else{
		$('.web-container').show();
		$('.web-movil').hide();
	}
}
var map;
function initMap() {
	// map = new google.maps.Map(document.getElementById('map'), {
	// 	center: {lat: -34.397, lng: 150.644},
	// 	zoom: 8
	// });
}

function isMovil() {
	var movil = false;
	if (screen.width <= 1025) {
		movil = true;
	}
	return movil;
}

function validaMail(email)
{
	var reg = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
	if (reg.test(email)){
		return true;
	}
	else{
		return false;
	}
}


function shoWContactoModal(){
	$('#mensajeRespuesta').empty().hide();
	$('#modalContacto').modal('show') ;
}