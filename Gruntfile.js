module.exports = function(grunt) {

    // 1. All configuration goes here
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
      concat_css: {
        options: {},
        front: {
          src: [
          // 'node_modules/animate.css/animate.min.css',
          'node_modules/aos/dist/aos.css',
          'css/style.css',
          ],
          dest: "compiled/css/front.css"
        },
      },
      concat: {
        front:{
         src: [
         'node_modules/aos/dist/aos.js',
         // 'node_modules/wow.js/dist/wow.min.js',
         'js/*.js'
         ],
         dest: "compiled/js/front.js"
       }
     },
     sass: {
      dist: {
        options: {                       // Target options
          style: 'expanded'
        },
        files: {
          'compiled/css/front.sass.css': 'sass/front-style.scss',
          'compiled/css/admin.sass.css': 'sass/admin-style.scss',
          'compiled/css/front-responsive-style.sass.css': 'sass/front-responsive-style.scss',
          'compiled/css/admin-responsive-style.sass.css': 'sass/admin-responsive-style.scss'
        }
      }
    },
    watch: {
      scripts: {
        files: [
        'css/*.css','bower_components/*/*','js/*.js','sass/*.scss'
        ],
        tasks: ['concat','concat_css','sass'],
        options: {
          spawn: true,
          livereload: true,
        },
      }
    }

  });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    //grunt.loadNpmTasks('grunt-contrib-imagemin');
    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['concat','concat_css','sass','watch']);

  };
